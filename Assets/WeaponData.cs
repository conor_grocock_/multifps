﻿using UnityEngine;
using System.Collections;

public class WeaponData : MonoBehaviour {

	public enum Weapons {
		Pistol,
		RPG,
		Sniper,
		MachineGun
	};
	
	public Weapons weapon;

	public float fireRate = 0.5f;
	public float damage = 25f;

	// Use this for initialization
	void Update () {
		if(weapon == Weapons.MachineGun){
			fireRate = 0.01f;
			damage = 5f;
		}else if(weapon == Weapons.Pistol){
			fireRate = 0.5f;
			damage = 10f;
		}else if(weapon == Weapons.RPG){
			fireRate = 5f;
			damage = 200f;
		}else if(weapon == Weapons.Sniper){
			fireRate = 2.5f;
			damage = 100f;
		}else{
			fireRate = float.MaxValue;
			damage = 0f;
		}
	}
}
