﻿using UnityEngine;
using System.Collections;

public class WeaponPickup : MonoBehaviour {

	public enum Weapons {
		Pistol,
		RPG,
		Sniper,
		MachineGun
	};
	
	public Weapons weapon;

	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter(Collision col) {
		if(col.gameObject.name == "SniperPickup")
		{
			this.gameObject.GetComponentInChildren<WeaponData>().weapon = WeaponData.Weapons.Sniper;
		}
	}
}
